import problemaGas.ProbGasBoard;
import problemaGas.ProbGasGoalTest;
import problemaGas.ProbGasHeuristicFunction;
import problemaGas.ProbGasSuccesorFunction;
import aima.search.framework.Problem;
import aima.search.framework.Search;
import aima.search.framework.SearchAgent;
import aima.search.informed.SimulatedAnnealingSearch;
import aima.search.informed.HillClimbingSearch;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import problemaGas.LinesComponent;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JFrame;


public class Main {

    public static void main(String[] args) throws Exception{
       
        int nCentresDistribucio;
        int nGasolineras;
        int generadorinicial;
        Random rnd = new Random();
        int seed = 1234;//rnd.nextInt();
        boolean SA;
        
        
        nCentresDistribucio = 10;
        nGasolineras = 100;
        generadorinicial=0;
        System.out.println(seed);
        int steps=500000;
        int spiter=5000;
        int k=125;
        double lamb;
        lamb = 0.0001;
        SA = false;


        ProbGasBoard board = new ProbGasBoard(nCentresDistribucio,1,seed, nGasolineras,SA);
        if(generadorinicial==1) board.GeneradorInicial1();
        if(generadorinicial==2) board.GeneradorInicial2();
        
        // Create the Problem object
        Problem p = new  Problem(board,
                                new ProbGasSuccesorFunction(),
                                new ProbGasGoalTest(),
                                new ProbGasHeuristicFunction());

        // Instantiate the search algorithm
        Search alg;
        if(SA) alg = new SimulatedAnnealingSearch(steps,spiter,k,lamb);
        else alg = new HillClimbingSearch();
        
        long startTime = System.currentTimeMillis();
        // Instantiate the SearchAgent object
        SearchAgent agent = new SearchAgent(p, alg);

        long endTime = System.currentTimeMillis();
	// We print the results of the search
        System.out.println();
        if(! SA)printActions(agent.getActions());
        printInstrumentation(agent.getInstrumentation());

        // You can access also to the goal state using the
	// method getGoalState of class Search
        
        ProbGasBoard board2 = (ProbGasBoard) alg.getGoalState();
        System.out.println("benefici: " + (-board2.benefici()));
        System.out.println("benefici real: " + (-board2.beneficioreal()));
        System.out.println("heuristic: "+ (-board2.heuristic()));
        System.out.println("peticions: "+ board2.numPeticions());
        System.out.println("peticions ateses: "+ board2.peticionsAteses());
        System.out.println("time: " + (endTime - startTime) + "ms");
        
        ArrayList<ProbGasBoard.Peticion> Peticiones = board2.getPeticiones();
        
        ArrayList<ProbGasBoard.Ruta> Rutas = board2.getRutas();
        
        ArrayList<ProbGasBoard.Camion> Camiones = board2.getCamiones();
        
        
        JFrame testFrame = new JFrame();
        testFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        final LinesComponent comp = new LinesComponent();
        comp.setPreferredSize(new Dimension(700, 700));
        testFrame.getContentPane().add(comp, BorderLayout.CENTER);    
        
        int x1,x2,y1,y2;
        Color randomColor;
        
        for (int i = 0; i < Rutas.size(); ++i){
            randomColor = new Color((float)Math.random(), (float)Math.random(), (float)Math.random());
            
            if(Rutas.get(i).Peticion1!=-1){
                x1 = 7*board2.getxcentro(Camiones.get(Rutas.get(i).indexCamion).indexCentro);
                x2 = 7*board2.getxgas(Rutas.get(i).Peticion1);
                y1 = 7*board2.getycentro(Camiones.get(Rutas.get(i).indexCamion).indexCentro);
                y2 = 7*board2.getygas(Rutas.get(i).Peticion1);
            
                comp.addLine(x1, y1, x2, y2, randomColor);
            }
            if(Rutas.get(i).Peticion2!=-1){
                x1 = 7*board2.getxcentro(Camiones.get(Rutas.get(i).indexCamion).indexCentro);
                x2 = 7*board2.getxgas(Rutas.get(i).Peticion2);
                y1 = 7*board2.getycentro(Camiones.get(Rutas.get(i).indexCamion).indexCentro);
                y2 = 7*board2.getygas(Rutas.get(i).Peticion2);
            
                comp.addLine(x1, y1, x2, y2, randomColor);
            }
            if(Rutas.get(i).Peticion1!=-1 && Rutas.get(i).Peticion2!=-1){
                x1 = 7*board2.getxgas(Rutas.get(i).Peticion2);
                x2 = 7*board2.getxgas(Rutas.get(i).Peticion1);
                y1 = 7*board2.getygas(Rutas.get(i).Peticion2);
                y2 = 7*board2.getygas(Rutas.get(i).Peticion1);
            
                comp.addLine(x1, y1, x2, y2, randomColor);
                           
            }
                        
        }
        
        for (int i = 0; i < Peticiones.size(); ++i){
            int cr1 = 7*board2.getxgas(i);
            int cr2 = 7*board2.getygas(i);
            comp.addLine(cr1-1, cr2-1, cr1+1, cr2+1);
            comp.addLine(cr1-1, cr2+1, cr1+1, cr2-1);
        }
        
        for (int i = 0; i < Camiones.size(); ++i){
            int cr1 = 7*board2.getxcentro(Camiones.get(i).indexCentro);
            int cr2 = 7*board2.getycentro(Camiones.get(i).indexCentro);
            comp.addLine(cr1, cr2-2, cr1, cr2+2);
            comp.addLine(cr1-2, cr2, cr1+2, cr2);
        }
        
        
            
       
        testFrame.pack();
        testFrame.setVisible(true);
        
    }

    

    private static void printInstrumentation(Properties properties) {
        Iterator keys = properties.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            String property = properties.getProperty(key);
            System.out.println(key + " : " + property);
        }
        
    }
    
    private static void printActions(List actions) {
        for (int i = 0; i < actions.size(); i++) {
            String action = (String) actions.get(i);
            System.out.println(action);
        }
    }
    
}