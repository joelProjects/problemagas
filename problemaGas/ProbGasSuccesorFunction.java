package problemaGas;

import aima.search.framework.SuccessorFunction;
import aima.search.framework.Successor;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bejar on 17/01/17
 */
public class ProbGasSuccesorFunction implements SuccessorFunction{

    public List getSuccessors(Object state){
        ArrayList retval = new ArrayList();
        ProbGasBoard board = (ProbGasBoard) state;
        ArrayList<ProbGasBoard.Peticion> Peticiones = board.getPeticiones();
        ArrayList<ProbGasBoard.Ruta> Rutas = board.getRutas();
        ArrayList<ProbGasBoard.Camion> Camions = board.getCamiones();
        
        if(ProbGasBoard.SA){
            int psina=0;
            for (int i = 0; i < Peticiones.size(); ++i){
                if (Peticiones.get(i).indexRuta == -1) ++psina;
            }
            int pasi=Peticiones.size() - psina;
            double pafegir=psina*Rutas.size();
            double pintercambiar= (pasi*(pasi-1))/2;
            double pinterruta= (pasi/2 * pasi/2)/2;
            double psubs=pasi*psina;
            double total=pafegir+pintercambiar+pinterruta+psubs;
            double p = Math.random();
            if(p<pafegir/total){
                //System.out.println("afegir "+  p + " "+ (pafegir)/total);
                int iruta= (int)(Math.random()*Rutas.size());
                int ipeticio= (int)(Math.random()*Peticiones.size());
                for (int i = ipeticio ; i != (ipeticio - 1 + Peticiones.size())% Peticiones.size() ; i= (i+1) % Peticiones.size()){

                    ProbGasBoard.Peticion Pi = Peticiones.get(i);

                    if (Pi.indexRuta == -1){ // la peticio no esta atesa?

                        for (int j = iruta ; j != (iruta - 1 + Rutas.size())% Rutas.size() ; j= (j+1) % Rutas.size()){ //afegirPeticioRuta                     
                            if ((Rutas.get(j).Peticion1 == -1 && 
                                board.calculaKm(Camions.get(Rutas.get(j).indexCamion).indexCentro, i, Rutas.get(j).Peticion2) <= 
                                board.calculaKm(Camions.get(Rutas.get(j).indexCamion).indexCentro, Rutas.get(j).Peticion1,  Rutas.get(j).Peticion2) + 
                                Camions.get(Rutas.get(j).indexCamion).KLibres) || 
                                (Rutas.get(j).Peticion2 == -1 && 
                                board.calculaKm(Camions.get(Rutas.get(j).indexCamion).indexCentro, i, Rutas.get(j).Peticion1) <=
                                board.calculaKm(Camions.get(Rutas.get(j).indexCamion).indexCentro, Rutas.get(j).Peticion1,  Rutas.get(j).Peticion2) + 
                                Camions.get(Rutas.get(j).indexCamion).KLibres))
                            {
                                ProbGasBoard new_state = board.copyOf();
                                new_state.afegirPeticioRuta(i, j);
                                retval.add(new Successor("afegir (P" + i + ", R" + j + ")", new_state));
                                return retval;
                            }
                        }
                    }
                }
                retval.add(new Successor("igual", board));
                return retval;
                
            }
            else if(p<=(pafegir+pintercambiar)/total){
                //System.out.println("inter "+  p + " "+ (pintercambiar)/total);
                int ipeticio2= (int)(Math.random()*Peticiones.size());
                int ipeticio= (int)(Math.random()*Peticiones.size());
                for (int i = ipeticio ; i != (ipeticio - 1 + Peticiones.size())% Peticiones.size() ; i= (i+1) % Peticiones.size()){

                    ProbGasBoard.Peticion Pi = Peticiones.get(i);
                    if (Pi.indexRuta != -1){ // la peticio no esta atesa?
                        for (int j = ipeticio2 ; j != (ipeticio2 - 1 + Peticiones.size())% Peticiones.size() ; j= (j+1) % Peticiones.size()){ //subtituir
                            if (j != i && Peticiones.get(j).indexRuta != Pi.indexRuta){
                                ProbGasBoard.Peticion Pj = Peticiones.get(j);
                                if (Pj.indexRuta != -1){ // Pj ja esta assignada?
                                    ProbGasBoard.Ruta Ri = Rutas.get(Pi.indexRuta);
                                    ProbGasBoard.Ruta Rj = Rutas.get(Pj.indexRuta);
                                    int Ci = Camions.get(Ri.indexCamion).indexCentro;
                                    int Cj = Camions.get(Rj.indexCamion).indexCentro;
                                    int Km_Ci = Camions.get(Ri.indexCamion).KLibres;
                                    int Km_Cj = Camions.get(Rj.indexCamion).KLibres;
                                    int Km_Ri_Ci = board.calculaKm(Ci, Ri.Peticion1, Ri.Peticion2);
                                    int Km_Rj_Cj = board.calculaKm(Cj, Rj.Peticion1, Rj.Peticion2);
                                    int PRj, PRi;
                                    if (Rj.Peticion1 == j)  PRj = Rj.Peticion2;
                                    else                    PRj = Rj.Peticion1;
                                    if (Ri.Peticion1 == i)  PRi = Ri.Peticion2;
                                    else                    PRi = Ri.Peticion1;
                                    int Km_i_Cj = board.calculaKm(Cj, i, PRj);
                                    int Km_j_Ci = board.calculaKm(Ci, j, PRi);
                                    if (Km_j_Ci <= Km_Ri_Ci + Km_Ci && Km_i_Cj <= Km_Rj_Cj + Km_Cj)
                                    { 
                                        ProbGasBoard new_state = board.copyOf();
                                        new_state.intercambiarPeticion(i, Pi.indexRuta, j, Pj.indexRuta);
                                        retval.add(new Successor("intercanviar (P" + i + ", R" + Pi.indexRuta + ", P" + j + ", R" + Pj.indexRuta + ")", new_state));
                                        return retval;
                                    }
                                }
                            }
                        }
                    }
                }
                retval.add(new Successor("igual", board));
                return retval;
                
            }
            else if(p<=(pafegir+pintercambiar+pinterruta)/total) {
                //System.out.println("interru "+  p + " "+ (pinterruta)/total);
                int iruta= (int)(Math.random()*Rutas.size());
                int iruta2= (int)(Math.random()*Rutas.size());
                for (int i = iruta ; i != (iruta - 1 + Rutas.size())% Rutas.size() ; i= (i+1) % Rutas.size()){
                    ProbGasBoard.Ruta Ri = Rutas.get(i);
                    for (int j = iruta2 ; j != (iruta2 - 1 + Rutas.size())% Rutas.size() ; j= (j+1) % Rutas.size()){
                        if (i != j){        // no mateixa Ruta?
                            ProbGasBoard.Ruta Rj = Rutas.get(j);
                            if (Ri.indexCamion != Rj.indexCamion){ // diferent camio?
                                if (Ri.Peticion1 != -1 || Ri.Peticion2 != -1 || Rj.Peticion1 != -1 || Rj.Peticion2 != -1){
                                    if ((board.calculaKm(Camions.get(Ri.indexCamion).indexCentro, Rj.Peticion1, Rj.Peticion2) <=
                                        board.calculaKm(Camions.get(Ri.indexCamion).indexCentro, Ri.Peticion1, Ri.Peticion2)  +
                                        Camions.get(Ri.indexCamion).KLibres)                                                &&
                                        (board.calculaKm(Camions.get(Rj.indexCamion).indexCentro, Ri.Peticion1, Ri.Peticion2) <=
                                        board.calculaKm(Camions.get(Rj.indexCamion).indexCentro, Rj.Peticion1, Rj.Peticion2)  +
                                        Camions.get(Rj.indexCamion).KLibres))
                                    {
                                        ProbGasBoard new_state = board.copyOf();
                                        new_state.intercanviarRuta(i, j);
                                        retval.add(new Successor("intercanviarRutes(R" + i + ", R" + j + ")", new_state));
                                        return retval;
                                    }
                                }
                            }
                        }
                    }
                }    
                retval.add(new Successor("igual", board));
                return retval;                
            }
            else if(p<=(pafegir+pintercambiar+pinterruta+psubs)/total) {
                //System.out.println("subn "+  p + " "+ (psubs)/total);
                int ipeticio2= (int)(Math.random()*Peticiones.size());
                int ipeticio= (int)(Math.random()*Peticiones.size());
                for (int i = ipeticio ; i != (ipeticio - 1 + Peticiones.size())% Peticiones.size() ; i= (i+1) % Peticiones.size()){

                    ProbGasBoard.Peticion Pi = Peticiones.get(i);
                    if (Pi.indexRuta == -1){ // la peticio no esta atesa?
                        for (int j = ipeticio2 ; j != (ipeticio2 - 1 + Peticiones.size())% Peticiones.size() ; j= (j+1) % Peticiones.size()){ //subtituir
                            ProbGasBoard.Peticion Pj = Peticiones.get(j);
                            if (Pj.indexRuta != -1){ // Pj ja esta assignada?
                                if ((Rutas.get(Pj.indexRuta).Peticion1 == j &&  // hi ha prou km lliures?
                                    board.calculaKm(Camions.get(Rutas.get(Pj.indexRuta).indexCamion).indexCentro, i, Rutas.get(Pj.indexRuta).Peticion2) <= 
                                    board.calculaKm(Camions.get(Rutas.get(Pj.indexRuta).indexCamion).indexCentro, j,  Rutas.get(Pj.indexRuta).Peticion2) + 
                                    Camions.get(Rutas.get(Pj.indexRuta).indexCamion).KLibres) || 
                                    (Rutas.get(Pj.indexRuta).Peticion2 == j && 
                                    board.calculaKm(Camions.get(Rutas.get(Pj.indexRuta).indexCamion).indexCentro, i, Rutas.get(Pj.indexRuta).Peticion1) <= 
                                    board.calculaKm(Camions.get(Rutas.get(Pj.indexRuta).indexCamion).indexCentro, j,  Rutas.get(Pj.indexRuta).Peticion1) + 
                                    Camions.get(Rutas.get(Pj.indexRuta).indexCamion).KLibres))
                                { 
                                    ProbGasBoard new_state = board.copyOf();
                                    new_state.substituirPeticion(i, j, Pj.indexRuta);
                                    retval.add(new Successor("subtituir (P" + i + ", P" + j + ", R" + Pj.indexRuta + ")", new_state));
                                    return retval;
                                }
                            }
                        }
                    }
                }
                retval.add(new Successor("igual", board));
                return retval;
            }
            else System.out.println("ERROR");
        } 
        else {

            
            

            for (int i = 0; i < Peticiones.size(); ++i){

                ProbGasBoard.Peticion Pi = Peticiones.get(i);

                if (Pi.indexRuta == -1){ // la peticio no esta atesa?

                    for (int j = 0; j < Rutas.size(); ++j){ //afegirPeticioRuta                     
                        if ((Rutas.get(j).Peticion1 == -1 && 
                            board.calculaKm(Camions.get(Rutas.get(j).indexCamion).indexCentro, i, Rutas.get(j).Peticion2) <= 
                            board.calculaKm(Camions.get(Rutas.get(j).indexCamion).indexCentro, Rutas.get(j).Peticion1,  Rutas.get(j).Peticion2) + 
                            Camions.get(Rutas.get(j).indexCamion).KLibres) || 
                            (Rutas.get(j).Peticion2 == -1 && 
                            board.calculaKm(Camions.get(Rutas.get(j).indexCamion).indexCentro, i, Rutas.get(j).Peticion1) <=
                            board.calculaKm(Camions.get(Rutas.get(j).indexCamion).indexCentro, Rutas.get(j).Peticion1,  Rutas.get(j).Peticion2) + 
                            Camions.get(Rutas.get(j).indexCamion).KLibres))
                        {
                            ProbGasBoard new_state = board.copyOf();
                            new_state.afegirPeticioRuta(i, j);
                            retval.add(new Successor("afegir (P" + i + ", R" + j + ")", new_state));
                        }
                    }

                    for (int j = 0; j < Peticiones.size(); ++j){ //subtituir
                        ProbGasBoard.Peticion Pj = Peticiones.get(j);
                        if (Pj.indexRuta != -1){ // Pj ja esta assignada?
                            if ((Rutas.get(Pj.indexRuta).Peticion1 == j &&  // hi ha prou km lliures?
                                board.calculaKm(Camions.get(Rutas.get(Pj.indexRuta).indexCamion).indexCentro, i, Rutas.get(Pj.indexRuta).Peticion2) <= 
                                board.calculaKm(Camions.get(Rutas.get(Pj.indexRuta).indexCamion).indexCentro, j,  Rutas.get(Pj.indexRuta).Peticion2) + 
                                Camions.get(Rutas.get(Pj.indexRuta).indexCamion).KLibres) || 
                                (Rutas.get(Pj.indexRuta).Peticion2 == j && 
                                board.calculaKm(Camions.get(Rutas.get(Pj.indexRuta).indexCamion).indexCentro, i, Rutas.get(Pj.indexRuta).Peticion1) <= 
                                board.calculaKm(Camions.get(Rutas.get(Pj.indexRuta).indexCamion).indexCentro, j,  Rutas.get(Pj.indexRuta).Peticion1) + 
                                Camions.get(Rutas.get(Pj.indexRuta).indexCamion).KLibres))
                            { 
                                ProbGasBoard new_state = board.copyOf();
                                new_state.substituirPeticion(i, j, Pj.indexRuta);
                                retval.add(new Successor("subtituir (P" + i + ", P" + j + ", R" + Pj.indexRuta + ")", new_state));
                            }
                        }
                    }                
                }
                else{ // (la peticio esta atesa)

                    /*for (int j = 0; j < Rutas.size(); ++j){ //mover
                        if (j != Pi.indexRuta){
                            //int pastKilometros = board.calculaKm(Camions.get(Rutas.get(Pi.indexRuta).indexCamion).indexCentro, Rutas.get(j).Peticion1, Rutas.get(j).Peticion2);
                            if (Rutas.get(j).Peticion1 == -1)
                            {
                                ProbGasBoard.Ruta Rj = Rutas.get(j);
                                int Cj = Camions.get(Rj.indexCamion).indexCentro;
                                int Km_Cj = Camions.get(Rj.indexCamion).KLibres;
                                int Km_Rj = board.calculaKm(Cj, Rj.Peticion1, Rj.Peticion2);
                                int Km_Pi = board.calculaKm(Cj, i, Rj.Peticion2);
                                if (Km_Pi <= Km_Rj + Km_Cj)
                                {
                                ProbGasBoard new_state = board.copyOf();
                                new_state.moverPeticion(i, Pi.indexRuta, j);
                                //if(Camions.get(Rutas.get(Pi.indexRuta).indexCamion).KLibres >=0)
                                retval.add(new Successor("moure (P" + i + ", R" + Pi.indexRuta + ", R" + j + ")", new_state));
                                }
                            }

                            else if (Rutas.get(j).Peticion2 == -1)
                            {
                                ProbGasBoard.Ruta Rj = Rutas.get(j);
                                int Cj = Camions.get(Rj.indexCamion).indexCentro;
                                int Km_Cj = Camions.get(Rj.indexCamion).KLibres;
                                int Km_Rj = board.calculaKm(Cj, Rj.Peticion1, Rj.Peticion2);
                                int Km_Pi = board.calculaKm(Cj, i, Rj.Peticion1);
                                if (Km_Pi <= Km_Rj + Km_Cj)
                                {
                                ProbGasBoard new_state = board.copyOf();
                                new_state.moverPeticion(i, Pi.indexRuta, j);
                                //if(Camions.get(Rutas.get(Pi.indexRuta).indexCamion).KLibres >=0)
                                retval.add(new Successor("moure (P" + i + ", R" + Pi.indexRuta + ", R" + j + ")", new_state));
                                }
                            }

                        }                    
                    }*/

                 
                    for (int j = i+1; j < Peticiones.size(); ++j){ // intercanviar
                        if (j != i && Peticiones.get(j).indexRuta != Pi.indexRuta){
                            ProbGasBoard.Peticion Pj = Peticiones.get(j);
                            if (Pj.indexRuta != -1){ // Pj ja esta assignada?
                                ProbGasBoard.Ruta Ri = Rutas.get(Pi.indexRuta);
                                ProbGasBoard.Ruta Rj = Rutas.get(Pj.indexRuta);
                                int Ci = Camions.get(Ri.indexCamion).indexCentro;
                                int Cj = Camions.get(Rj.indexCamion).indexCentro;
                                int Km_Ci = Camions.get(Ri.indexCamion).KLibres;
                                int Km_Cj = Camions.get(Rj.indexCamion).KLibres;
                                int Km_Ri_Ci = board.calculaKm(Ci, Ri.Peticion1, Ri.Peticion2);
                                int Km_Rj_Cj = board.calculaKm(Cj, Rj.Peticion1, Rj.Peticion2);
                                int PRj, PRi;
                                if (Rj.Peticion1 == j)  PRj = Rj.Peticion2;
                                else                    PRj = Rj.Peticion1;
                                if (Ri.Peticion1 == i)  PRi = Ri.Peticion2;
                                else                    PRi = Ri.Peticion1;
                                int Km_i_Cj = board.calculaKm(Cj, i, PRj);
                                int Km_j_Ci = board.calculaKm(Ci, j, PRi);
                                if (Km_j_Ci <= Km_Ri_Ci + Km_Ci && Km_i_Cj <= Km_Rj_Cj + Km_Cj)
                                { 
                                    ProbGasBoard new_state = board.copyOf();
                                    new_state.intercambiarPeticion(i, Pi.indexRuta, j, Pj.indexRuta);
                                    //if(Camions.get(Rutas.get(Pi.indexRuta).indexCamion).KLibres >=0 && Camions.get(Rutas.get(Pj.indexRuta).indexCamion).KLibres >=0){
                                    retval.add(new Successor("intercanviar (P" + i + ", R" + Pi.indexRuta + ", P" + j + ", R" + Pj.indexRuta + ")", new_state));


                                }
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < Rutas.size(); ++i){
                ProbGasBoard.Ruta Ri = Rutas.get(i);
                for (int j = i+1; j < Rutas.size(); ++j){
                    if (i != j){        // no mateixa Ruta?
                        ProbGasBoard.Ruta Rj = Rutas.get(j);
                        if (Ri.indexCamion != Rj.indexCamion){ // diferent camio?
                            if (Ri.Peticion1 != -1 || Ri.Peticion2 != -1 || Rj.Peticion1 != -1 || Rj.Peticion2 != -1){
                                if ((board.calculaKm(Camions.get(Ri.indexCamion).indexCentro, Rj.Peticion1, Rj.Peticion2) <=
                                    board.calculaKm(Camions.get(Ri.indexCamion).indexCentro, Ri.Peticion1, Ri.Peticion2)  +
                                    Camions.get(Ri.indexCamion).KLibres)                                                &&
                                    (board.calculaKm(Camions.get(Rj.indexCamion).indexCentro, Ri.Peticion1, Ri.Peticion2) <=
                                    board.calculaKm(Camions.get(Rj.indexCamion).indexCentro, Rj.Peticion1, Rj.Peticion2)  +
                                    Camions.get(Rj.indexCamion).KLibres))
                                {
                                    ProbGasBoard new_state = board.copyOf();
                                    new_state.intercanviarRuta(i, j);
                                    retval.add(new Successor("intercanviarRutes(R" + i + ", R" + j + ")", new_state));

                                }
                            }
                        }
                    }
                }
            }     

        }
        return retval;
    }

}
