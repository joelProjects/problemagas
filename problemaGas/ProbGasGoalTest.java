package problemaGas;

import aima.search.framework.GoalTest;

/**
 * Created by bejar on 17/01/17.
 */
public class ProbGasGoalTest implements GoalTest {

    public boolean isGoalState(Object state){

        return((ProbGasBoard) state).is_goal();
    }
}
