package problemaGas;

/**
 * Created by bejar on 17/01/17.
 */

import aima.search.framework.HeuristicFunction;

public class ProbGasHeuristicFunction implements HeuristicFunction {

    public double getHeuristicValue(Object n){

        return ((ProbGasBoard) n).heuristic();
    }
}
