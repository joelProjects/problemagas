package problemaGas;

import IA.Gasolina.Gasolinera;
import IA.Gasolina.Gasolineras;
import IA.Gasolina.CentrosDistribucion;
import IA.Gasolina.Distribucion;
import java.util.ArrayList;


public class ProbGasBoard {
    
    public class Peticion{
        public int indexGasolinera;      //index de la gasolinera que tiene la peticion(para saber la posicion)
        public int indexRuta;            //index de la ruta que la atiende(-1 si aun no esta atendida)
        public int dias;                 //dias que lleva sin atender la peticion (0=hoy)
        public Peticion(int indexG,int indexR, int days){
            indexGasolinera= indexG;
            indexRuta= indexR;
            dias= days;
        }
        public Peticion(Peticion p){
            indexGasolinera= p.indexGasolinera;
            indexRuta= p.indexRuta;
            dias= p.dias;
        }
    }
    
    public class Ruta{
        public int indexCamion;          //index del camion que hace la ruta
        public int Peticion1;            //index de la peticion1, -1 si no atiende ninguna
        public int Peticion2;            //index de la peticion2, -1 si no atiende ninguna
        public Ruta(int a, int b, int c){
            indexCamion= a;
            Peticion1= b;
            Peticion2=c;
        }
        public Ruta(Ruta r){
            indexCamion= r.indexCamion;
            Peticion1= r.Peticion1;
            Peticion2=r.Peticion2;
        }
    }
    
    public class Camion{
        public int indexCentro;           //index del centro de donde sale el camion(para la posicion)
        public int KLibres;                //kilometros libres
        public Camion(int a,int b){
            indexCentro= a;
            KLibres= b;
        }
        public Camion(Camion c){
            indexCentro= c.indexCentro;
            KLibres= c.KLibres;
        }
        
    }    
     
    private static CentrosDistribucion Centros;
    private static Gasolineras Gasos;
    private static int Maxkilometros;
    private static int Maxviajes;
    public static boolean SA;
    private ArrayList<Peticion> Peticiones; 
    private ArrayList<Ruta> Rutas; 
    private ArrayList<Camion> Camiones; 
    
    
    public void debug(){
        for(int i=0;i<Rutas.size();i++){
            Ruta r = Rutas.get(i);
            if(r.Peticion1!=-1 && Peticiones.get(r.Peticion1).indexRuta!=i)System.out.println("BUG!!");            
            if(r.Peticion2!=-1 && Peticiones.get(r.Peticion2).indexRuta!=i)System.out.println("BUG!!");
        }
        for(int i=0;i<Peticiones.size();i++){
            if(Peticiones.get(i).indexRuta!=-1 && (Rutas.get(Peticiones.get(i).indexRuta).Peticion1!=i && Rutas.get(Peticiones.get(i).indexRuta).Peticion2!=i))
                System.out.println("BUG2!!");
        }
    }

    /* Constructor */
    public ProbGasBoard(int ncen, int mult, int seed, int ngas,boolean SA_HC){
        
        SA=SA_HC;
        Centros = new CentrosDistribucion(ncen,mult,seed);
        Gasos = new Gasolineras(ngas,seed);
        Maxkilometros=640;//640
        Maxviajes=5;
        Peticiones = new ArrayList<>();
        Camiones = new ArrayList<>();
        Rutas = new ArrayList<>();
        for (int i = 0; i< ngas; i++) {
            ArrayList<Integer> p = (Gasos.get(i)).getPeticiones();
            for (int j = 0; j< p.size(); j++) {
                Peticiones.add( new Peticion(i,-1,p.get(j)));   
            }
        }
        for (int i = 0; i< ncen; i++) {
            Camiones.add( new Camion(i,Maxkilometros) );
            for(int j=0; j < Maxviajes; ++j){
                Rutas.add( new Ruta(i,-1,-1) );
            }
        }
            
    }
    
    public ProbGasBoard(ArrayList<Peticion> P, ArrayList<Ruta> R, ArrayList<Camion> C){
        Peticiones = new ArrayList<>();
        Camiones = new ArrayList<>();
        Rutas = new ArrayList<>();
        for(int i=0;i<P.size();++i){
            Peticiones.add( new Peticion(P.get(i)));
        }
        for(int i=0;i<R.size();++i){
            Rutas.add( new Ruta(R.get(i)));
        }
        for(int i=0;i<C.size();++i){
            Camiones.add( new Camion(C.get(i)));
        }   
    }
    
    public int calculaKm(int centro, int pet1, int pet2){
        int km = 0;
        if(pet1 == -1 && pet2 == -1) return km;
        Distribucion centroDist = Centros.get(centro);
        Gasolinera gas1, gas2;
        gas1= null;
        gas2= null;
        Peticion pet;
        if(pet1 != -1) {
            pet = Peticiones.get(pet1);
            gas1 = Gasos.get(pet.indexGasolinera);
        }
        if(pet2 != -1){
            pet = Peticiones.get(pet2);
            gas2 = Gasos.get(pet.indexGasolinera);
        }
        
        int x, y;
        if(pet1 != -1 && pet2 == -1){
            x = centroDist.getCoordX() - gas1.getCoordX();
            y = centroDist.getCoordY() - gas1.getCoordY();
            if(x < 0) x = -x;
            if(y < 0) y = -y;
            km = 2*(x + y);
        } 
        else if(pet1 == -1 && pet2 != -1) {
            x = centroDist.getCoordX() - gas2.getCoordX();
            y = centroDist.getCoordY() - gas2.getCoordY();
            if(x < 0) x = -x;
            if(y < 0) y = -y;
            km = 2*(x + y);
        }
        else if(pet1 != -1 && pet2 != -1){
            x = centroDist.getCoordX() - gas1.getCoordX();
            y = centroDist.getCoordY() - gas1.getCoordY();
            if(x < 0) x = -x;
            if(y < 0) y = -y;
            km += x + y;
            x = gas1.getCoordX() - gas2.getCoordX();
            y = gas1.getCoordY() - gas2.getCoordY();
            if(x < 0) x = -x;
            if(y < 0) y = -y;
            km += x + y;
            x = gas2.getCoordX() - centroDist.getCoordX();
            y = gas2.getCoordY() - centroDist.getCoordY();
            if(x < 0) x = -x;
            if(y < 0) y = -y;
            km += x + y;
        }
        return km;
    }

    //precondicion: la peticion no tiene asignada ninguna ruta, la ruta tiene assignada 
    //menos de dos peticiones y el camion tiene kilometros libres para atender esta peticion
    //postcondicion: a la ruta se le añade la peticion, la peticion pertenece a la ruta y al camion se
    //le actualizan los kimometros
    public void afegirPeticioRuta(int indexpeticion,int indexruta){
        Peticion petNova;
        Ruta route = Rutas.get(indexruta);
        Camion truck = Camiones.get(route.indexCamion);
       
        int pastKilometros = calculaKm(truck.indexCentro, route.Peticion1, route.Peticion2);        

        if(route.Peticion1 == -1){
            route.Peticion1 = indexpeticion;
        }
        else{
            route.Peticion2 = indexpeticion;
        }
        petNova = Peticiones.get(indexpeticion);
        petNova.indexRuta = indexruta;
        
        int kilometros = calculaKm(truck.indexCentro, route.Peticion1, route.Peticion2); 
        
        truck.KLibres = truck.KLibres + pastKilometros - kilometros;
    }
    
  
    
    //precondicion: ambas peticiones ya estan asignadas a rutas diferentes y la substitución no sobrepasa el límite de
    //kilometros de ninguno de los camiones, la peticion 1 pertenece a la ruta 1 y la peticion 2 a la ruta 2
    //postcondicion: a la ruta 1 se le substituye la peticion x por la peticion y, y a la ruta 2 se substituye
    //la petición y por la petición x, tambien se actualizan los kilómetros de los camiones en sus respectivas rutas
    public void intercambiarPeticion(int indexpeticion1,int indexruta1,int indexpeticion2,int indexruta2){
        Ruta route1 = Rutas.get(indexruta1);
        Ruta route2 = Rutas.get(indexruta2);
        
        //Recalculació de kilometres i reassignacions de peticions de la ruta 1
        Camion truck1 = Camiones.get(route1.indexCamion);
        int pastKilometros = calculaKm(truck1.indexCentro, route1.Peticion1, route1.Peticion2); 
        
        if(route1.Peticion1 == indexpeticion1){
            route1.Peticion1 = indexpeticion2;
        }
        else{
            route1.Peticion2 = indexpeticion2;
        }
        
        Peticion petNovaRuta1 = Peticiones.get(indexpeticion2);
        petNovaRuta1.indexRuta = indexruta1;
        
        int kilometros = calculaKm(truck1.indexCentro, route1.Peticion1, route1.Peticion2);
        truck1.KLibres = truck1.KLibres + pastKilometros - kilometros;
        
        //Recalculació de kilometres i reassignacions de rutes de la ruta 2
        Camion truck2 = Camiones.get(route2.indexCamion);
        pastKilometros = calculaKm(truck2.indexCentro, route2.Peticion1, route2.Peticion2); 
        
        if(route2.Peticion1 == indexpeticion2){
            route2.Peticion1 = indexpeticion1;
        }
        else{
            route2.Peticion2 = indexpeticion1;
        }
        
        Peticion petNovaRuta2 = Peticiones.get(indexpeticion1);
        petNovaRuta2.indexRuta = indexruta2;
        
        kilometros = calculaKm(truck2.indexCentro, route2.Peticion1, route2.Peticion2); 
        truck2.KLibres = truck2.KLibres + pastKilometros - kilometros;
    }
    
    //precondicion: la petición ya se encuentra asignada a una ruta i la ruta2 tiene un espacio libre como mínimo, 
    //la asignación de la ruta no sobrepasa el límite de kilometros del camión
    //postcondicion: a la ruta 2 se le añade la peticion y a la ruta 1 se le desasigna, también se actualizan los 
    //kilómetros de ambos camiones
    public void moverPeticion(int indexpeticion,int indexruta1,int indexruta2){
        Ruta route1 = Rutas.get(indexruta1);
        Ruta route2 = Rutas.get(indexruta2);
        
        Camion truck1 = Camiones.get(route1.indexCamion);
        Camion truck2 = Camiones.get(route2.indexCamion);
        
        //Recalculació de kilometres i reassignacions de peticions de la ruta 1
        int pastKilometros = calculaKm(truck1.indexCentro, route1.Peticion1, route1.Peticion2); 
        
        if(route1.Peticion1 == indexpeticion){
            route1.Peticion1 = -1;
        }
        else{
            route1.Peticion2 = -1;
        }
        
        int kilometros = calculaKm(truck1.indexCentro, route1.Peticion1, route1.Peticion2);
        truck1.KLibres = truck1.KLibres + pastKilometros - kilometros;
        
        //Recalculació de kilometres i reassignacions de rutes de la ruta 2
        pastKilometros = calculaKm(truck2.indexCentro, route2.Peticion1, route2.Peticion2); 
        
        if(route2.Peticion1 == -1){
            route2.Peticion1 = indexpeticion;
        }
        else{
            route2.Peticion2 = indexpeticion;
        }
        
        kilometros = calculaKm(truck2.indexCentro, route2.Peticion1, route2.Peticion2); 
        truck2.KLibres = truck2.KLibres + pastKilometros - kilometros;
        
        Peticion petNovaRuta = Peticiones.get(indexpeticion);
        petNovaRuta.indexRuta = indexruta2;
    }
    
    //precondicion: la petición 1 no se encuentra en ninguna ruta, la petición 2 ya se encuentra asignada
    //a la ruta dada, la substitución no sobrepasa el límite de kilómetros del camión
    //postcondicion: a la ruta se le substituye la petición 2 por la 1 y se actualizan los kilómetros del camión
    public void substituirPeticion(int indexpeticion1,int indexpeticion2,int indexruta){
        Ruta route = Rutas.get(indexruta);
        
        Camion truck = Camiones.get(route.indexCamion);
        
        //Recalculació de kilometres i reassignacions de peticions de la ruta 
        int pastKilometros = calculaKm(truck.indexCentro, route.Peticion1, route.Peticion2); 
        
        if(route.Peticion1 == indexpeticion2){
            route.Peticion1 = indexpeticion1;
        }
        else{
            route.Peticion2 = indexpeticion1;
        }
        
        int kilometros = calculaKm(truck.indexCentro, route.Peticion1, route.Peticion2);
        truck.KLibres = truck.KLibres + pastKilometros - kilometros;
        
        Peticion petNovaRuta = Peticiones.get(indexpeticion1);
        petNovaRuta.indexRuta = indexruta;
        
        petNovaRuta = Peticiones.get(indexpeticion2);
        petNovaRuta.indexRuta = -1;
    }
    
    //precondicion: la substitución no sobrepasa el límite de kilómetros de cap camión
    //postcondicion: es recalculen els kilòmetres dels camions i s'intercanvien les rutes entre els camions
    public void intercanviarRuta(int indexruta, int indexruta2){
        Ruta route1 = Rutas.get(indexruta);
        Ruta route2 = Rutas.get(indexruta2);
        
        Camion truck1 = Camiones.get(route1.indexCamion);
        Camion truck2 = Camiones.get(route2.indexCamion);
        
        int kmRoute1 = calculaKm(truck1.indexCentro, route1.Peticion1, route1.Peticion2);
        int kmRoute1new = calculaKm(truck1.indexCentro, route2.Peticion1, route2.Peticion2);
        int kmRoute2 = calculaKm(truck2.indexCentro, route2.Peticion1, route2.Peticion2);
        int kmRoute2new = calculaKm(truck2.indexCentro, route1.Peticion1, route1.Peticion2);
        
        truck1.KLibres = truck1.KLibres + kmRoute1 - kmRoute1new;
        truck2.KLibres = truck2.KLibres + kmRoute2 - kmRoute2new;
        
        int p1= route1.Peticion1;
        if(p1!=-1) Peticiones.get(p1).indexRuta=indexruta2;
        int p2= route1.Peticion2;
        if(p2!=-1) Peticiones.get(p2).indexRuta=indexruta2;        
        int p3= route2.Peticion1;
        if(p3!=-1) Peticiones.get(p3).indexRuta=indexruta;
        int p4= route2.Peticion2;        
        if(p4!=-1) Peticiones.get(p4).indexRuta=indexruta;
        
        route1.Peticion1=p3;
        route1.Peticion2=p4;
        route2.Peticion1=p1;
        route2.Peticion2=p2;
        
    }
    
    public double heuristic(){
        double beneficio = 0.;
        for (int i = 0; i < Peticiones.size(); ++i){
            Peticion Pi = Peticiones.get(i);
            if (Pi.indexRuta != -1){
                if (Pi.dias == 0) beneficio += 1020;
                else beneficio += (100- Math.pow(2, Pi.dias))*10;
            }
            else {
                if (Pi.dias == 0) beneficio -= 1020 - (100- 2)*10;
                else beneficio -= (100- Math.pow(2, Pi.dias))*10 - (100- Math.pow(2, Pi.dias+1))*10;
            }
        }
        for (int i = 0; i < Camiones.size(); ++i){
            Camion Ci = Camiones.get(i);
            beneficio -= (Maxkilometros-Ci.KLibres)*2;
        }
        return -beneficio;
    }
    
    public double heuristic2(){
        double beneficio = 0.;
        for (int i = 0; i < Peticiones.size(); ++i){
            Peticion Pi = Peticiones.get(i);
            if (Pi.indexRuta != -1){
                if (Pi.dias == 0) beneficio += 1020;
                else beneficio += (100- Math.pow(2, Pi.dias))*10;
            }
            else beneficio += (100- Math.pow(2, Pi.dias+1))*4;
        }
        for (int i = 0; i < Camiones.size(); ++i){
            Camion Ci = Camiones.get(i);
            beneficio -= (Maxkilometros-Ci.KLibres)*2;
        }
        return -beneficio;
    }
    
     public double greedyheur(){
        double beneficio = 0.;
        for (int i = 0; i < Peticiones.size(); ++i){
            Peticion Pi = Peticiones.get(i);
            if (Pi.indexRuta != -1){
                if (Pi.dias == 0) beneficio += 1020;
                else beneficio += (100- Math.pow(2, Pi.dias))*10;
            }
            else beneficio -= (100- Math.pow(2, Pi.dias+1))*10;
        }
        for (int i = 0; i < Camiones.size(); ++i){
            Camion Ci = Camiones.get(i);
            beneficio -= (Maxkilometros-Ci.KLibres)*2;
        }
        return -beneficio;
    }
    
    public double beneficioreal(){
        double beneficio = 0.;
        for (int i = 0; i < Peticiones.size(); ++i){
            Peticion Pi = Peticiones.get(i);
            if (Pi.indexRuta != -1){
                if (Pi.dias == 0) beneficio += 1020;
                else beneficio += (100- Math.pow(2, Pi.dias))*10;
            }
            else beneficio += (100- Math.pow(2, Pi.dias+1))*10;
        }
        for (int i = 0; i < Camiones.size(); ++i){
            Camion Ci = Camiones.get(i);
            beneficio -= (Maxkilometros-Ci.KLibres)*2;
        }
        return -beneficio;
    }
    
    public double benefici(){
        double beneficio = 0.;
        for (int i = 0; i < Peticiones.size(); ++i){
            Peticion Pi = Peticiones.get(i);
            if (Pi.indexRuta != -1){
                if (Pi.dias == 0) beneficio += 1020;
                else beneficio += (100- Math.pow(2, Pi.dias))*10;
            }
        }
        for (int i = 0; i < Camiones.size(); ++i){
            Camion Ci = Camiones.get(i);
            beneficio -= (Maxkilometros-Ci.KLibres)*2;
        }
        return -beneficio;
    }
    
    public int peticionsAteses(){
        int npet = 0;
        for(int i = 0; i < Peticiones.size(); ++i){
            if(Peticiones.get(i).indexRuta != -1) ++npet;        
        }
        return npet;        
    }
    
    public int numPeticions(){
        return Peticiones.size();
    }

    public void GeneradorInicial1(){
        for (int i = 0; i < Peticiones.size(); ++i){            
                for (int j = 0; j < Rutas.size() && Peticiones.get(i).indexRuta==-1; ++j){ //afegirPeticioRuta                     
                    if ((Rutas.get(j).Peticion1 == -1 && 
                        this.calculaKm(Camiones.get(Rutas.get(j).indexCamion).indexCentro, i, Rutas.get(j).Peticion2) <= 
                        this.calculaKm(Camiones.get(Rutas.get(j).indexCamion).indexCentro, Rutas.get(j).Peticion1,  Rutas.get(j).Peticion2) + 
                        Camiones.get(Rutas.get(j).indexCamion).KLibres) || 
                        (Rutas.get(j).Peticion2 == -1 && 
                        this.calculaKm(Camiones.get(Rutas.get(j).indexCamion).indexCentro, i, Rutas.get(j).Peticion1) <=
                        this.calculaKm(Camiones.get(Rutas.get(j).indexCamion).indexCentro, Rutas.get(j).Peticion1,  Rutas.get(j).Peticion2) + 
                        Camiones.get(Rutas.get(j).indexCamion).KLibres))
                    {
                        this.afegirPeticioRuta(i, j);
                        System.out.println("afegir (P" + i + ", R" + j + ")");
                    }
                }           
        }
    }
    public void GeneradorInicial2(){
        for (int i = 0; i < Peticiones.size(); ++i){            
                for (int j = 0; j < Rutas.size() && Peticiones.get(i).indexRuta==-1; ++j){ //afegirPeticioRuta                     
                    if ((Rutas.get(j).Peticion1 == -1 && 
                        this.calculaKm(Camiones.get(Rutas.get(j).indexCamion).indexCentro, i, Rutas.get(j).Peticion2) <= 
                        this.calculaKm(Camiones.get(Rutas.get(j).indexCamion).indexCentro, Rutas.get(j).Peticion1,  Rutas.get(j).Peticion2) + 
                        Camiones.get(Rutas.get(j).indexCamion).KLibres) )
                    {
                        this.afegirPeticioRuta(i, j);
                        System.out.println("afegir (P" + i + ", R" + j + ")");
                    }
                }           
        }
    }
    


    /* Goal test */
    public boolean is_goal(){
        return false;
    }

     /* auxiliary functions */
    
    public ArrayList<Peticion> getPeticiones () {
        return Peticiones;
    }
    
    public ArrayList<Ruta> getRutas () {
        return Rutas;
    }
    
    public ArrayList<Camion> getCamiones () {
        return Camiones;
    }
    
    public int getxcentro (int centro) {        
        return Centros.get(centro).getCoordX();        
    }
    public int getycentro (int centro) {        
        return Centros.get(centro).getCoordY();        
    }
    public int getxgas (int pet) {        
        return Gasos.get(Peticiones.get(pet).indexGasolinera).getCoordX();        
    }
    public int getygas (int pet) {        
        return Gasos.get(Peticiones.get(pet).indexGasolinera).getCoordY();           
    }
    
    public ProbGasBoard copyOf() {
        ProbGasBoard newBoard = new ProbGasBoard(Peticiones, Rutas, Camiones);
        return newBoard;
    }

}